const axios = require('axios');
const { App, AwsLambdaReceiver } = require('@slack/bolt');
require('dotenv').config();

const awsLambdaReceiver = new AwsLambdaReceiver({
    signingSecret: process.env.SLACK_SIGNING_SECRET,
});

const app = new App({
    token: process.env.SLACK_BOT_TOKEN,
    receiver: awsLambdaReceiver,
});

const slackOptions = { headers: { Authorization: 'Bearer ' + process.env.SLACK_BOT_TOKEN }};
const refreshRequest = {
    'client_id': process.env.GOOGLE_CLIENT_ID,
    'client_secret': process.env.GOOGLE_CLIENT_SECRET,
    'refresh_token': process.env.GOOGLE_REFRESH_TOKEN,
    'grant_type': 'refresh_token',
};

app.event('file_shared', async ({ event, client, logger }) => {
    let imageMetadata = await axios.get('https://slack.com/api/files.info?file=' + event.file_id, slackOptions);
    let imageData = await axios.get(imageMetadata.data.file.url_private, { responseType: 'arraybuffer', ...slackOptions });
    imageData = Buffer.from(imageData.data, 'binary');
    let refreshToken = await axios.post('https://oauth2.googleapis.com/token', refreshRequest);
    let googleOptions = { headers: { Authorization: 'Bearer ' + refreshToken.data.access_token }};
    let uploadToken = await axios.post('https://photoslibrary.googleapis.com/v1/uploads', imageData, googleOptions);
    let uploadData = {
        "albumId": process.env.ALBUM_ID,
        "newMediaItems": [{
            "description": "Auto-uploaded from Slack #pictures-together",
            "simpleMediaItem": {
                "fileName": imageMetadata.data.file.title,
                "uploadToken": uploadToken.data,
            },
        }],
    };
    let mediaItem = await axios.post('https://photoslibrary.googleapis.com/v1/mediaItems:batchCreate', uploadData, googleOptions);
    return { statusCode: 200, data: 'Success' };
});

module.exports.slackHandler = async (event, context, callback) => {
    const handler = await awsLambdaReceiver.start();
    return handler(event, context, callback);
}

module.exports.googleHandler = async (event, context, callback) => {
    console.log(event.queryStringParameters.code);
    callback(null, 'Hello, world!');
}
