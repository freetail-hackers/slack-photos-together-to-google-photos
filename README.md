# Slack #pictures-together to Google Photos

This is a Slack bot that automatically uploads images posted to the #pictures-together channel to an album in the admin@freetailhackers.com Google Photos account. It requires the following environment variables to be defined in `.env`:

```
SLACK_SIGNING_SECRET=
SLACK_BOT_TOKEN=
GOOGLE_CLIENT_ID=
GOOGLE_CLIENT_SECRET=
GOOGLE_REFRESH_TOKEN=
ALBUM_ID=
```

The Slack variables can be retrieved by following Slack's documentation for bots. The Google variables can be retrieved by following Google's documentation for authorizing with an OAuth flow. The album ID must be retrieved by creating an album through the Google Photos web interface and then making an API call to get its ID.

## Development

Install dependencies with `npm install` and deploy with `serverless deploy` (you need Node.js and the Serverless Framework).
